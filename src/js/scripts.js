function article(title, image, text) {
    this.title = title;
    this.image = image;
    this.text = text;
}
function getRandom(){
  let min = Math.ceil(1);
  let max = Math.floor(5);
  return Math.floor(Math.random() * (max - min)) + min;
}
function returnPageNumberHTML(id) {
    return "<div id='" + id + "' class='article__page_number' onclick='handlePageClickEvent(this)'>" + id + "</div>"
}

function renderArticleHTML(i, article) {
    return "<div id='article" + i + "' class='article'><div class='thumbnail'><img src='../img/articles/" + i + ".jpeg'></div><div class='article__info'><div> <h1>" + article.title + "</h1><p class='thumbnail__date'>" + article.date + "</p></div><p id='articleContent" + i + "'>" + article.content.slice(0, 80) + " ...</p><img id='buttonReadMore_" + i + "' class='article__button' onclick='handleReadMoreButton(this)' src='./img/button.png'/></div> </div>"
}
var data;
var randomNumber;
var numberOfPages;
var articlePagePicker;
let highlightNodes;
var ar = new article('title', 'thumb', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');
let articleNextButton = document.getElementById('article__page_picker_next');
let articlePrevButton = document.getElementById('article__page_picker_prev');
let articlesDOM = document.getElementById('articles');
window.onload = function() {
    randomNumber = getRandom()
    showRandomSlide(randomNumber);
    var countries = ['?', 'Sweden', 'Denmark', '', 'Sweden', 'Norway', 'Norway', 'Denmark'];
    for (let i = 0; i < countries.length; i++) {
        if (countries[i] != '') {
            document.getElementById('winnersSidebar').innerHTML =
                document.getElementById('winnersSidebar').innerHTML + "<div class='sidebar__table_row'><div class='sidebar__table_collumn1'>" + (2011 - i).toString() + "</div><div class='sidebar__table_collumn2'>-</div><div class='sidebar__table_collumn3'>" + countries[i] + "</div> </div>"
        }
    }

    //events
    var menuButton = document.getElementById('menu');
    menuButton.addEventListener('click', function() {
        var menuWrapper = document.getElementById('menuWrapper');
        menuWrapper.classList.add('open')
    });
    var closeButton = document.getElementById('closeButton');
    closeButton.addEventListener('click', function() {
        var menuWrapper = document.getElementById('menuWrapper');
        menuWrapper.classList.remove('open');
        menuWrapper.classList.add('close');
        setTimeout(function() {
            document.getElementById('menuWrapper').classList.remove('close');
        }, 1000)
    });
    var input = document.getElementById('search')
    input.addEventListener('input', function(e) {})
    var request = new XMLHttpRequest();

    request.open('GET', '../csvjson.json', true);


    request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
            // Success!
            data = JSON.parse(request.responseText);
            articlesHTML = '';

            var date = new Date();
            var dateFormat = date.getDate() + '/' + parseInt(date.getMonth()) + 1 + '-' + date.getFullYear();

            for (var i = 1; i <= 3; i++) {
                articlesDOM.innerHTML = articlesDOM.innerHTML + renderArticleHTML(i, data[i]);

            }
            numberOfPages = (data.length - (data.length % 3)) / 3 + 1;
            articlePagePicker = document.getElementById('article__page_picker');
            for (let i = 1; i <= 3; i++) {
                articlePagePicker.innerHTML += returnPageNumberHTML(i);
            }
            var pageNumber = (new URL(document.location)).searchParams.get('page');
            if (pageNumber == null) {
                pageNumber = 1
            }
            document.getElementById(pageNumber).classList.add('article__page_number-active')
            document.getElementById('article__page_info').innerHTML = 'Page ' + pageNumber + ' of ' + numberOfPages;
            document.getElementById('article__page_picker_prev').classList.add('hide');
        } else {
            // We reached our target server, but it returned an error

        }
    };

    request.onerror = function() {
        // There was a connection error of some sort
    };

    request.send();
};

function handlePageClickEvent(e) {
    //baseCase
    let renderedNumbers = document.getElementsByClassName('article__page_number');
    for (let i = 0; i < renderedNumbers.length; i++) {
        renderedNumbers[i].classList.remove('article__page_number-active');
    }

    let id = parseInt(e.id)
    if (id != 1 && id != numberOfPages) {
        articlePagePicker.innerHTML = '';
        for (let i = id - 1; i <= id + 1; i++) {
            articlePagePicker.innerHTML += returnPageNumberHTML(i);
        }

    }

    articlesDOM.innerHTML = '';
    let start = (id - 1) * 3;
    let end;
    if (id == numberOfPages) {
        end = start + ((numberOfPages) * 3 - data.length) - 2;

    } else {
        end = start + 2
    }
    for (let i = start; i <= end; i++) {
        articlesDOM.innerHTML += renderArticleHTML(i, data[i])
    }

    if (id != 1 && id != numberOfPages) {
        articlePrevButton.classList.remove('hide');
        articleNextButton.classList.remove('hide');
    } else if (id == 1) {
        articlePrevButton.classList.add('hide');
    } else {
        articleNextButton.classList.add('hide');
    }

    document.getElementById(id).classList.add('article__page_number-active');

}

function handleReadMoreButton(e) {
    let id = e.id.split('_')[1];
    let text = document.getElementById('articleContent' + id);
    text.innerHTML = data[id].content;
}

function handleInput(e){
  console.log(e.value);
  clearHighlight()
  let allNodes = document.getElementsByTagName('*');
  let replacement = document.createElement('span');
  let expression = new RegExp(e.value,'g');
  replacement.classList.add('highlight');
  replacement.innerText = e.value;
  for(let i = 0; i<allNodes.length;i++){
    if(allNodes[i].children.length == 0 && allNodes[i].innerText != '' && allNodes[i].tagName!='TITLE'&& allNodes[i].tagName!='SPAN'){
    //  console.log(allNodes[i].innerHTML.replace(expression,"<span style='highlight'>"+e.value+"</span>"));
    let newText = allNodes[i].innerHTML.replace(expression,"<span class='highlight'>"+e.value+"</span>");

    allNodes[i].innerHTML = newText;

    }
  }
}
function clearHighlight(){
  let allNodes = document.getElementsByTagName('*');
  let spanNodes = [];
  let counter = 0;
  for(let i = 0; i<allNodes.length; i++){
    console.log(allNodes[i].tagName,allNodes[i])
    if(allNodes[i].tagName == 'SPAN'){
      spanNodes.push(allNodes[i]);
    }
  }

  let expression;
  console.log(counter);
  if(spanNodes[0]!= undefined){
  //  console.log(spanNodes[0].outerHTML);
  //  expression = new RegExp(spanNodes[0].outerHTML);
    for(let i = 0; i<spanNodes.length; i++){
    //  console.log(spanNodes[i].parentElement.innerHTML.replace(expression, spanNodes[i].innerText));
    if(spanNodes[i].parentElement != null){
      spanNodes[i].parentElement.innerHTML=spanNodes[i].parentElement.innerHTML.replace(new RegExp(spanNodes[i].outerHTML,'g'), spanNodes[i].innerText)
    }
  }
  }

}
function showRandomSlide(number) {

    document.getElementById('slider'+number).classList.add('fadeIn');
}
function handleSlideClick(e){
  let allSliders = document.getElementsByClassName('slider__image');
  for(let i = 0; i<allSliders.length;i++){
    allSliders[i].classList.remove('fadeIn');
  }
  if(e.id == 'next'){
    randomNumber+=1;
  }
  if(e.id == 'prev'){
    randomNumber+=1;
  }
  if(randomNumber == 5){
    randomNumber = 1
  }
  if(randomNumber == 0){
    randomNumber = 4
  }
  document.getElementById('slider'+randomNumber).classList.add('fadeIn')
  ''('slider'+randomNumber+" "+randomNumber+" "+e.id)
}
//read json
